#!/usr/bin/env python3

"""
Runs coala on a given pull request and determines which results are new.
"""

import json
from os import environ
from os.path import relpath
from shutil import rmtree
from tempfile import mkdtemp
import sys
from copy import copy

from coalib.coala_main import run_coala
from coalib.output.printers.ListLogPrinter import ListLogPrinter
from coalib.results.RESULT_SEVERITY import RESULT_SEVERITY
from coalib.results.Result import Result
from coalib.results.ResultFilter import filter_results
from git.repo.base import Repo

from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.Interfaces.Commit import Commit
from IGitt.Interfaces.CommitStatus import CommitStatus, Status
from IGitt.Interfaces.MergeRequest import MergeRequest


def runit(project_dir):
    """
    Runs coala! Yeah!

    :param project_dir: The directory to run coala on.
    :return: A dictionary that holds a list of all results for all section
             names as well as another dictionary associating the same section
             names with file dicts.
    """
    log_printer = ListLogPrinter()

    oldargv = copy(sys.argv)
    sys.argv.clear()
    sys.argv.extend(['coala', '-c='+project_dir])
    results, _, file_dicts = run_coala(log_printer=log_printer, autoapply=False)
    sys.argv.clear()
    sys.argv.extend(oldargv)

    return results, file_dicts


def describe_patch(diffs, tempdir):
    patch = ""
    for filename, diff in diffs.items():
        filename = relpath(filename, tempdir)
        patch += "\n\n```diff\n"+diff.unified_diff.replace(
                "--- \n+++ \n",
                '--- a/'+filename+'\n+++ b/'+filename+'\n')+"```"
    return "\n\nThe issue can be fixed by applying the following patch:"+patch


def comment_results(commit: Commit, results: [Result], tempdir, mr_number):
    """
    Takes the results and comments them on the given commit.

    :param commit: A Commit object.
    :param results: The new results (dict).
    """
    for section_name in results:
        if len(results[section_name]) > 3:
            commit.comment('There are {} results for the section {}. Run coala '
                           'locally to see them.'.format(
                               str(len(results[section_name]), section_name),
                           None, None, mr_number=mr_number)
            continue
        for result in results[section_name]:
            file = None
            line = None
            if result.affected_code:
                file = relpath(result.affected_code[0].start.file,
                               start=tempdir)
                line = result.affected_code[0].start.line

            patch = ("" if result.diffs is None
                     else describe_patch(result.diffs, tempdir))

            commit.comment(
                ("{message}\n"
                 "\n"
                 "*{origin}, severity {severity}, section `{section}`.*{patch}")
                .format(message=result.message, origin=result.origin,
                        severity=RESULT_SEVERITY.__str__(result.severity),
                        section=section_name, patch=patch),
                file, line, mr_number=mr_number)


def get_new_results(old_file_dicts, file_dicts, old_results, results):
    """
    Retrieves new results.

    :param old_file_dicts: Dict holding file dicts for all sections
    :param file_dicts: Dict holding file dicts for all sections
    :param old_results: Dict holding results for all sections
    :param results: Dict holding results for all sections
    :return: Dict holding all new results for the sections.
    """
    new_results = {}
    for section_name in results:
        if section_name not in old_results:  # New section
            new_results[section_name] = results[section_name]
            continue

        # Workaround for https://github.com/coala-analyzer/coala/issues/1866
        affected_files = set(old_file_dicts[section_name].keys())
        affected_files |= set(file_dicts[section_name].keys())
        for file in affected_files:
            if file not in old_file_dicts[section_name]:
                old_file_dicts[section_name][file] = []
            if file not in file_dicts[section_name]:
                file_dicts[section_name][file] = []

        new_results[section_name] = filter_results(
            old_file_dicts[section_name], file_dicts[section_name],
            old_results[section_name], results[section_name])

    return new_results


def analyse_commits(git_repo: Repo, proj_dir: str, pr: MergeRequest,
                    omit_comments: bool):
    """
    Runs coala over all commits.

    :param git_repo: The repository for checking out the commits.
    :param proj_dir: The directory where the cloned repository is.
    :param pr: A merge request instance so we can iterate over the commits. All
               commits are expected to be present in the repository.
    """
    commit_infos = {}

    # Set everything to in progress
    pr.head.set_status(CommitStatus(
        Status.PENDING, "GitMate analysis in progress...",
        "review/gitmate/pr", 'http://gitmate.com/'))
    for commit in pr.commits:
        commit.set_status(CommitStatus(
            Status.PENDING, "GitMate analysis in progress...",
            "review/gitmate/commit", 'http://gitmate.com/'))

    git_repo.git.checkout(pr.base.sha)
    old_results, old_file_dicts = runit(proj_dir)
    commit_infos[pr.base.sha] = {
        'count': sum(len(section) for section in old_results.values())}
    all_is_fine = True

    for commit in pr.commits:
        git_repo.git.checkout(commit.sha)
        results, file_dicts = runit(proj_dir)
        commit_infos[commit.sha] = {
            'count': sum(len(section) for section in results.values())}

        new_results = get_new_results(old_file_dicts, file_dicts, old_results,
                                      results)
        if any(new_results[section] for section in new_results):
            if all_is_fine:
                all_is_fine = False
                pr.head.set_status(CommitStatus(
                    Status.FAILED, "This PR is not ready yet :/",
                    "review/gitmate/pr", 'http://gitmate.com/'))

            if not omit_comments:
                comment_results(commit, new_results, proj_dir, pr._number)

            commit.set_status(CommitStatus(
                Status.FAILED, "Issues found! Sorry :(",
                "review/gitmate/commit", 'http://gitmate.com/'))
        else:
            commit.set_status(CommitStatus(
                Status.SUCCESS, "No issues with this one - go ahead! :)",
                "review/gitmate/commit", 'http://gitmate.com/'))

        old_results, old_file_dicts = results, file_dicts

    new_results = (commit_infos[pr.base.sha]['count'] -
                   commit_infos[pr.head.sha]['count'])
    if all_is_fine:
        pr.head.set_status(CommitStatus(
            Status.SUCCESS, "All is well! :) ({} problems solved)".format(
                new_results),
            "review/gitmate/pr", 'http://gitmate.com/'))
    else:
        pr.head.set_status(CommitStatus(
            Status.FAILED,
            "This PR is not ready yet :/ ({} new problems)".format(
                -new_results),
            "review/gitmate/pr", 'http://gitmate.com/'))

    return commit_infos


def main():
    """
    Nothing here yet.
    """
    token = environ['OAUTH_TOKEN']
    repo_name = environ['REPO_NAME']
    pr_number = environ['PR_NUMBER']
    omit_comments = environ.get('OMIT_COMMENTS', "False").lower() != "false"
    pr = GitHubMergeRequest(token, repo_name, pr_number)
    repo = GitHubRepository(token, repo_name)

    tempdir = mkdtemp()
    commit_infos = {}
    errors = []
    try:
        git_repo = Repo.clone_from(repo.clone_url, tempdir)
        try:
            git_repo.remote("origin").fetch('refs/pull/{}/head'.format(pr_number))
        except:
            pass  # FIXME, find a better solution

        commit_infos = analyse_commits(git_repo, tempdir, pr, omit_comments)
    except Exception as exc:
        errors = [str(exc)]
    finally:
        rmtree(tempdir)

    print(json.dumps({'commits': commit_infos, 'errors': errors}))


if __name__ == '__main__':
    main()
